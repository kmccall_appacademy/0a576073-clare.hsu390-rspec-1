
  def echo(word)
    word
  end

  def shout(string)
    string.upcase
  end

  def repeat(string, num = 2)
    ([string] * num).join(" ")
  end

  def start_of_word(word, num)
    word[0...num]
  end

  def first_word(string)
    string.split[0]
  end

  def titleize(string)

    words_no_cap = ["and", "or", "the", "over", "to", "a", "but"]
    string.split.map.with_index do |word, idx|
      if words_no_cap.include?(word) && idx != 0
         word
      else
        word.capitalize
      end
    end.join(" ")
end
