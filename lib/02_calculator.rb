def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(numbers)
  numbers.reduce(:*)
end

def power(num1, num2)
  num1 ** num2
end

def factorial(number)
  n = 1
  while number > n
    number *= n
    n += 1
  end
  number
end
