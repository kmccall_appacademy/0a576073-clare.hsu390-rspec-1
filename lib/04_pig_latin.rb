def pig_latinize(word)
  vowels = "aeiou"
  until vowels.include?(word[0])
    word = word[1..-1] + word[0]
      if word[-1] == "q" && word[0] == "u"
        word = word[1..-1] + word[0]
      end 
  end

  word + "ay"
end

def translate(string)
  new_arr = [ ]
  string.split.each do |word|
    new_arr <<  pig_latinize(word)
  end
  new_arr.join(" ")
end
